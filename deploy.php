<?php

function parseArgs($argv)
{
    array_shift($argv); $o = array();
    foreach ($argv as $a){
        if (substr($a,0,2) == '--'){ $eq = strpos($a,'=');
            if ($eq !== false){ $o[substr($a,2,$eq-2)] = substr($a,$eq+1); }
            else { $k = substr($a,2); if (!isset($o[$k])){ $o[$k] = true; } } }
        else if (substr($a,0,1) == '-'){
            if (substr($a,2,1) == '='){ $o[substr($a,1,1)] = substr($a,3); }
            else { foreach (str_split(substr($a,1)) as $k){ if (!isset($o[$k])){ $o[$k] = true; } } } }
        else { $o[] = $a; } }
    return $o;
}

$arguments = parseArgs($argv);

$host = $arguments['host'] ?? null;
$user = $arguments['username'] ?? null;
$password = $arguments['password'] ?? null;
$dir = $arguments['dir'] ?? null;
$file = $arguments['file'] ?? null;
$hash = $arguments['hash'] ?? null;

//establish connection
echo "Connecting...\n";

$ssh = ssh2_connect($host);
if(ssh2_auth_password($ssh, $user, $password) === false) {
    echo "Authentication Failed!\n";
    exit(1);
}

echo "Connected!\n";

//check if current commit is already deployed
$stream = ssh2_exec($ssh, 'if test -d '.$dir.'/'.$hash.'; then echo "exist"; fi');
stream_set_blocking($stream, true);
$stream_out = ssh2_fetch_stream($stream, SSH2_STREAM_STDIO);
if(trim(stream_get_contents($stream_out)) == 'exist') {
    echo "Commit already deployed!\n";
    exit(1);
}

echo "Sending Archive...\n";

//send file to host
if(!file_exists($file)) {
    echo "File not found!\n";
    exit(1);
}

$filename = basename($file);
ssh2_scp_send($ssh, $file, '/tmp/'.$filename);

echo "Archive sent!\n";

//extract
echo "Extracting...\n";

ssh2_exec($ssh, 'mkdir -p '.$dir.'/'.$hash);
ssh2_exec($ssh, 'tar -xf '.'/tmp/'.$filename.' -C '.$dir.'/'.$hash);
ssh2_exec($ssh, 'rm '.$dir.'/current');
ssh2_exec($ssh, 'ln -s '.$dir.'/'.$hash.' '.$dir.'/current');

echo "Extracted!\n";

//dir permissions
ssh2_exec($ssh, 'chown -R www-data:www-data '.$dir);
ssh2_exec($ssh, 'chmod -R 775 '.$dir);

echo "======================\nFINISHED\n";
exit(0);